package Robot;

import Utilities.U_Time;
import jssc.SerialPort;
import jssc.SerialPortException;

/**
 * Created by Apple on 2017/7/6 AD.
 */
public class AutRobot {
    private SerialPort portfd;
    private int motorSpeedLeft;
    private int motorSpeedRight;
    private int motorShaftLeft;
    private int motorShaftRight;
    private double omegaL;
    private double  omegaR;
    private int sonarRear;
    private int sonarRearL;
    private int sonarRearR;
    private int sonarFront;
    private int sonarFrontL;
    private int sonarFrontR;
    private U_Time updateTime;
    private double battery;
    private int reset;

    public SerialPort getPortfd() {
        return portfd;
    }

    public void setPortfd(SerialPort portfd) {
        this.portfd = portfd;
    }

    public int getMotorSpeedLeft() {
        return motorSpeedLeft;
    }

    public void setMotorSpeedLeft(int motorSpeedLeft) {
        this.motorSpeedLeft = motorSpeedLeft;
    }

    public int getMotorSpeedRight() {
        return motorSpeedRight;
    }

    public void setMotorSpeedRight(int motorSpeedRight) {
        this.motorSpeedRight = motorSpeedRight;
    }

    public int getMotorShaftLeft() {
        return motorShaftLeft;
    }

    public void setMotorShaftLeft(int motorShaftLeft) {
        this.motorShaftLeft = motorShaftLeft;
    }

    public int getMotorShaftRight() {
        return motorShaftRight;
    }

    public void setMotorShaftRight(int motorShaftRight) {
        this.motorShaftRight = motorShaftRight;
    }

    public double getOmegaL() {
        return omegaL;
    }

    public void setOmegaL(double omegaL) {
        this.omegaL = omegaL;
    }

    public double getOmegaR() {
        return omegaR;
    }

    public void setOmegaR(double omegaR) {
        this.omegaR = omegaR;
    }

    public int getSonarRear() {
        return sonarRear;
    }

    public void setSonarRear(int sonarRear) {
        this.sonarRear = sonarRear;
    }

    public int getSonarRearL() {
        return sonarRearL;
    }

    public void setSonarRearL(int sonarRearL) {
        this.sonarRearL = sonarRearL;
    }

    public int getSonarRearR() {
        return sonarRearR;
    }

    public void setSonarRearR(int sonarRearR) {
        this.sonarRearR = sonarRearR;
    }

    public int getSonarFront() {
        return sonarFront;
    }

    public void setSonarFront(int sonarFront) {
        this.sonarFront = sonarFront;
    }

    public int getSonarFrontL() {
        return sonarFrontL;
    }

    public void setSonarFrontL(int sonarFrontL) {
        this.sonarFrontL = sonarFrontL;
    }

    public int getSonarFrontR() {
        return sonarFrontR;
    }

    public void setSonarFrontR(int sonarFrontR) {
        this.sonarFrontR = sonarFrontR;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public int getReset() {
        return reset;
    }

    public void setReset(int reset) {
        this.reset = reset;
    }

    public AutRobot(){
        updateTime = new U_Time();
    }

    public SerialPort connect(AutRobot autRobot){
        SerialPort serialPort = new SerialPort("/dev/ttyUSB0");
        try {
            serialPort.openPort();
        }catch (SerialPortException e){
            e.printStackTrace();
        }
        this.setPortfd(serialPort);
        return serialPort;
    }

    public int updateRobot(AutRobot autRobot){
        int i;
        int[] data = new int[8];
        char[] command = new char[20];
        
        return 0;
    }
}
