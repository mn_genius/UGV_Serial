package Utilities;


/**
 * Created by Apple on 2017/7/6 AD.
 */
public class U_Time {
    private int sec;
    private int uSec;

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public int getuSec() {
        return uSec;
    }

    public void setuSec(int uSec) {
        this.uSec = uSec;
    }
}
